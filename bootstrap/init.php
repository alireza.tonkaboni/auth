<?php
session_start();
date_default_timezone_set('Asia/Tehran');
require_once 'constants.php';
require_once './vendor/autoload.php';
require_once 'config.php';
require_once './libs/helpers.php';
require_once './libs/auth-lib.php';


// اتصال به پایگاه داده
try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname;charset=$charset", $user, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // echo "Connect is successful";
} catch (PDOException $e) {
    // echo "Error: " . $e->getMessage();
    die();
}


