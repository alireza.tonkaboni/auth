<?php

function is_user_exists(string $email = null,string $phone = null):bool {
    global $conn;
    $sql = 'SELECT * FROM `users` WHERE `email`= :email OR `phone`= :phone';
    $stmt = $conn->prepare($sql);
    $stmt->execute([':email'=>$email ?? '',':phone'=>$phone ?? '']);
    $record = $stmt->fetch(PDO::FETCH_OBJ);
    return $record ? TRUE : FALSE;
}

function create_user(array $params):bool {
    global $conn;
    $sql = 'INSERT INTO `users` (`name`,`phone`,`email`) VALUES (:name, :phone, :email)';
    $stmt = $conn->prepare($sql);
    $stmt->execute([':name'=>$params['name'],':phone'=>$params['phone'],':email'=>$params['email']]);
    return $stmt->rowCount() ? TRUE : FALSE;
}

function create_login_token():array {
    global $conn;
    $hash = bin2hex(random_bytes(8));
    $token = rand(100000,999999);
    $expired_at = time() + 600 ;
    $sql = 'INSERT INTO `tokens` (`token`,`hash`,`expired_at`) VALUES (:token, :hash, :expired_at)';
    $stmt = $conn->prepare($sql);
    $stmt->execute([':token'=>$token,':hash'=>$hash,':expired_at'=>date('Y-m-d H:i:s',$expired_at)]);
    return ['token'=>$token,'hash'=>$hash];
}

function is_alive_token(string $hash):bool {
    $record = find_token_by_hash($hash);
    if (!$record) return FALSE;
    return strtotime($record->expired_at) > time() + 120;
}

function find_token_by_hash(string $hash):object|Bool {
    global $conn;
    $sql = 'SELECT * FROM `tokens` WHERE `hash`= :hash';
    $stmt = $conn->prepare($sql);
    $stmt->execute([':hash'=>$hash]);
    $record = $stmt->fetch(PDO::FETCH_OBJ);
    return $record;
}

function send_token_by_mail(string $email , string|int $token):bool{
    global $mail;
    $mail->setFrom('auth@tonkaboni.ir', 'Alireza Tonkaboni');
    $mail->addAddress($email);
    $mail->isHTML(true);
    $mail->Subject = 'Here is the subject';
    $mail->Body    = 'Your Token is : <b>'.$token.'</b>';
    $mail->send();
    return true;
}