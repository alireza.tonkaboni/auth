<?php

require_once './bootstrap/init.php';


try {
    $mail->setFrom('auth@tonkaboni.ir', 'Alireza Tonkaboni');
    $mail->addAddress('alireza.tnkbn@gmail.com', 'Alireza Tonkaboni');
    $mail->isHTML(true);
    $mail->Subject = 'Here is the subject';
    $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}