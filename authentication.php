<?php

require_once "bootstrap/init.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $action = $_GET['action'];
    $params = $_POST;
    if($action=='register') {
        if (empty($params['name']) || empty($params['phone']) || empty($params['email'])) {
            set_error_and_redirect("All input fields are required!", "authentication.php?action=register");
        }
    }
    if(is_user_exists($params['email'],$params['phone'])){
        set_error_and_redirect("User exists! Please login :)", "authentication.php?action=login");
    }
    if(!filter_var($params['email'],FILTER_VALIDATE_EMAIL)){
        set_error_and_redirect("Please enter a valid email!", "authentication.php?action=register");
    }
    if(create_user($params)){
        $_SESSION['email']=$params['email'];
        redirect("authentication.php?action=verify");
    }
}

echo "<pre>";
print_r($_POST);
echo "</pre>";

if(isset($_GET['action']) && $_GET['action']== 'verify' && !empty($_SESSION['email'])) {
    if(!is_user_exists($_SESSION['email'])){
        set_error_and_redirect("User NOT exists! Please Register :)", "authentication.php?action=register");
    }
    if(isset($_SESSION['hash']) && is_alive_token($_SESSION['hash'])) {
        # send old token
    }else {
        $token_result = create_login_token();
        $_SESSION['hash'] = $token_result['hash'];
    }
    include './template/verify-tpl.php';
}

if(isset($_GET['action']) && $_GET['action']== 'register') {
    include './template/register-tpl.php';
}else{
    include './template/login-tpl.php';
}
